<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $fillable = [
        'projectName' , 'projectDescription' ,'startDate' , 'endDate' , 'projectOwner' , 
         'user_id' , 'projectStatus', 'deleted_at', 'created_at', 'updated_at'
    ];

    public function task(){
        return $this->hasMany(Task::class);
    }

}
