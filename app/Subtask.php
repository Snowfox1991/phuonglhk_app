<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtask extends Model
{
    protected $fillable = [
        'subtaskName','estimateHour','actualHour','member_id','task_id','user_id','taskStatus',
        'isActive','isDeleted', 'created_at','updated_at','deleted_at'
    ];

    public function tasks(){
        return $this->belongsTo(Task::class);
    }
}
