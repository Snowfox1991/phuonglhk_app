<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class PassportController extends Controller
{

         /**
 * @SWG\Swagger(
 *      schemes={"http", "https"},
 *      @SWG\Info(
 *          version="1.0.0",
 *          title="Project Management API Documentation",
 *          description="This is the document of our team for generating API Concept Documentation.",
 *          @SWG\Contact(
 *              email="volibear153@gmail.com"
 * ),
 *      )
 *  )
 *
 */



    /**
     * @SWG\Post(
     *   path="/api/register",
     *   summary="Register a user",
     *   operationId="register",
     *   tags={"User"},
     *   security={
     *       {"Bearer": {}}
     *   },
     *   @SWG\Parameter(
     *       name="userId",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Parameter(
     *       name="fullName",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Parameter(
     *       name="email",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *  @SWG\Parameter(
     *       name="password",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful"
     *   ),
     *   @SWG\Response(
     *       response="400",
     *       description="BAD REQUEST"
     *   ),
     *   @SWG\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *   @SWG\Response(
     *       response="409",
     *       description="Conflict"
     *   ),
     *   @SWG\Response(
     *       response="500",
     *       description="Internal Server Error"
     *     ),
     *     security={{"Bearer":{}}}
     * )
     * )
     *
     */

    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'fullName' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
            'userId' => $request->get('userId'),
            'fullName' => $request->get('fullName'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'isActived' => true,
        ]);

        $token = $user->createToken('Snowfox1991')->accessToken;

        return response()->json(['token' => $token,
        'result' => 'Success'], 201);
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

     /**
     * @SWG\Post(
     *   path="/api/login",
     *   summary="Login a user",
     *   operationId="login",
     *   tags={"User"},
     *   security={
     *       {"Bearer": {}}
     * },
     *   @SWG\Parameter(
     *       name="email",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *  @SWG\Parameter(
     *       name="password",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful"
     *   ),
     *   @SWG\Response(
     *       response="400",
     *       description="BAD REQUEST"
     *   ),
     *   @SWG\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *   @SWG\Response(
     *       response="409",
     *       description="Conflict"
     *   ),
     *   @SWG\Response(
     *       response="500",
     *       description="Internal Server Error"
     *     )
     * )
     * )
     *
     */


    public function login(Request $request)
    {
        // $request['isActived'] = TRUE;
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'isActived' => true
        ];

        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('Granted')->accessToken;
            return response()->json(['token' => $token], 201);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */

     /**
 * @SWG\SecurityScheme(
 *   securityDefinition="Bearer",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization",
 * )
 */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }
}
