<?php

namespace App\Http\Controllers;
use App\Subtask;

use Illuminate\Http\Request;

class SubtaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subtask = auth()->user()->subtask_task();

        // if(!$subtask){
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Subtasks are empty.!'
        //     ]);
        // }

        return response()->json([
            'success' => true,
            'data' => $task
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subtaskName' => 'required',
            'estimateHour' => 'required',
            'task_id' => 'required|exists:tasks,id',
            'member_id' => 'exists:members,id',
            'taskStatus' => 'required',

        ]);

        $subtask = new Subtask();
        $subtask->subtaskName = $request->subtaskName;
        $subtask->estimateHour = $request->estimateHour;
        $subtask->actualHour = $request->actualHour;
        $subtask->task_id = $request->task_id;
        $subtask->member_id = $request->member_id;
        $subtask->taskStatus = $request->taskStatus;

        if (auth()->user()->subtasks()->save($subtask))
            return response()->json([
                'success' => true,
                'data' => $subtask->toArray()
            ], 201);
        else
            return response()->json([
                'success' => false,
                'message' => 'Subtask could not be added'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subtask = auth()->user()->subtask()->find($id);

        if (!$subtask) {
            return response()->json([
                'success' => false,
                'message' => 'Subtask with id ' . $id . ' not found'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $subtask->toArray()
        ], 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subtask = auth()->user()->subtask()->find($id);

        if (!$subtask) {
            return response()->json([
                'success' => false,
                'message' => 'Subtask with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $subtask->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true,
                'data' => $subtask
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Tasks could not be updated'
            ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subtask = auth()->user()->subtask()->find($id);

        if (!$tasks) {
            return response()->json([
                'success' => false,
                'message' => 'Subtask with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $subtask->update([
            'deleted_at' => Carbon::NOW(),
            'isActived' => false,
            'isDeleted' => true,
            'taskStatus' => 'CANCELLED'
            ]);

        if ($updated)
            return response()->json([
                'success' => true,
                'data' => $subtask
            ], 200);
        else
            return response()->json([
                'success' => false,
                'message' => 'Subtask could not be deleted'
            ], 400);
    }

    public function getSubtaskByTask()
    {
        $tasks = auth()->user()->subtask()->where('task_id' , request()->task_id)->get();

        if (!$tasks) {
            return response()->json([
                'success' => false,
                'message' => 'Subtask with task id ' . $task_id . ' not found'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $tasks->toArray()
        ], 200);
    }
}

