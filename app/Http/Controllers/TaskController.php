<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Projects;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = auth()->user()->task_project();

        // if(!$task){
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Tasks are empty.!'
        //     ]);
        // }


        return response()->json([
            'success' => true,
            'data' => $task
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //statuses ['NOT STARTED','IN PROGRESSING','COMPLETED','DECLINED','CANCELLED']
        $this->validate($request, [
            'taskName' => 'required',
            'estimateHour' => 'required',
            'project_id' => 'required|exists:projects,id',
            'member_id' => 'exists:members,id',
            'statuses' => 'required',

        ]);

        $task = new Task();
        $task->taskName = $request->taskName;
        $task->estimateHour = $request->estimateHour;
        $task->actualHour = $request->actualHour;
        $task->project_id = $request->project_id;
        $task->member_id = $request->member_id;
        $task->statuses = $request->statuses;

        if (auth()->user()->tasks()->save($task))
            return response()->json([
                'success' => true,
                'data' => $task->toArray()
            ], 201);
        else
            return response()->json([
                'success' => false,
                'message' => 'Task could not be added'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = auth()->user()->tasks()->find($id);

        if (!$task) {
            return response()->json([
                'success' => false,
                'message' => 'Tasks with id ' . $id . ' not found'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $task->toArray()
        ], 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tasks = auth()->user()->tasks()->find($id);

        if (!$tasks) {
            return response()->json([
                'success' => false,
                'message' => 'Task with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $tasks->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true,
                'data' => $tasks
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Tasks could not be updated'
            ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tasks = auth()->user()->tasks()->find($id);

        if (!$tasks) {
            return response()->json([
                'success' => false,
                'message' => 'Tasks with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $tasks->update([
            'deleted_at' => Carbon::NOW(),
            'isActived' => false,
            'isDeleted' => true,
            'statuses' => 'CANCELLED'
            ]);

        if ($updated)
            return response()->json([
                'success' => true,
                'data' => $tasks
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Tasks could not be deleted'
            ], 500);
    }


    public function getTaskByProject()
    {
        $tasks = auth()->user()->tasks()->where('project_id' , request()->project_id)->get();

        if (!$tasks) {
            return response()->json([
                'success' => false,
                'message' => 'Task with project id ' . $project_id . ' not found'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $tasks->toArray()
        ], 200);
    }


    // public function getTaskByProject(){
    //     $query = auth()->user()->getTaskByProjectid();

    //     return response()->json([
    //         'success' => true,
    //         'data' => $query
    //     ]);
    // }
}
