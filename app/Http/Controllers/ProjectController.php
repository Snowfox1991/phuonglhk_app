<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;
use Carbon\Carbon;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     /**
     * @SWG\Get(
     *     path="/api/projects",
     *     summary="Get projects",
     *     description="Get projects",
     *     operationId="index",
     * tags={"Projects"},
     *     security={
     *       {"Bearer": {}}
     *   },
     *   @SWG\Response(
     *     response=200,
     *     description="Successful"
     *   ),
     *   @SWG\Response(
     *       response="400",
     *       description="BAD REQUEST"
     *   ),
     *   @SWG\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *   @SWG\Response(
     *       response="409",
     *       description="Conflict"
     *   ),
     *   @SWG\Response(
     *       response="500",
     *       description="Internal Server Error"
     *     )
     * )
     * )
     */
    public function index()
    {
        $projects = auth()->user()->projects;

        return response()->json([
            'success' => true,
            'data' => $projects
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Post(
     *   path="/api/projects",
     *   summary="Create A Project",
     *   operationId="store",
     *   tags={"Projects"},
     *   security={
     *       {"Bearer": {}}
     *   },
     *   @SWG\Parameter(
     *       name="projectName",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Parameter(
     *       name="projectDescription",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Parameter(
     *       name="startDate",
     *       in="formData",
     *       required=true,
     *       type="integer"
     *   ),
     *  @SWG\Parameter(
     *       name="endDate",
     *       in="formData",
     *       required=true,
     *       type="integer"
     *   ),
     *  @SWG\Parameter(
     *       name="projectOwner",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     * *  @SWG\Parameter(
     *       name="projectStatus",
     *       in="formData",
     *       required=true,
     *       type="string"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="Successful"
     *   ),
     *   @SWG\Response(
     *       response="400",
     *       description="BAD REQUEST"
     *   ),
     *   @SWG\Response(
     *       response="401",
     *       description="Unauthorized"
     *     ),
     *   @SWG\Response(
     *       response="409",
     *       description="Conflict"
     *   ),
     *   @SWG\Response(
     *       response="500",
     *       description="Internal Server Error"
     *     ),
     * )
     * )
     *
     */

    public function store(Request $request)
    {
        //chú ý chỗ này sẽ là form exception nha
        //required là bắt buộc phải có thông tin nhập, k có là nó k cho chạy
        //projectStatus để dạng enum string gồm
        //['NOT STARTED', 'IN PROGRESS', 'COMPLETED', 'FAILED']
        $this->validate($request, [
            'projectName' => 'required',
            'projectDescription' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'projectOwner' => 'required',
            'projectStatus' => 'required',

        ]);

        $projects = new Projects();
        $projects->projectName = $request->projectName;
        $projects->projectDescription = $request->projectDescription;
        $projects->startDate = $request->startDate;
        $projects->endDate = $request->endDate;
        $projects->projectOwner = $request->projectOwner;
        $projects->projectStatus = $request->projectStatus;

        if (auth()->user()->projects()->save($projects))
            return response()->json([
                'success' => true,
                'data' => $projects->toArray()
            ], 201);
        else
            return response()->json([
                'success' => false,
                'message' => 'Projects could not be added'
            ], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //   /**
    //  * @SWG\Get(
    //  *     path="/api/projects/{id}",
    //  *     summary="Get projects by id",
    //  *     description="Get projects by id",
    //  *     operationId="showProjectById",
    //  * tags={"Projects"},
    //  *     security={
    //  *       {"Bearer": {}}
    //  *   },
    //  * @SWG\Parameter(
    //  *       name="id",
    //  *       in="path",
    //  *       required=true,
    //  *       type="integer"
    //  *   ),
    //  *   @SWG\Response(
    //  *     response=200,
    //  *     description="Successful"
    //  *   ),
    //  *   @SWG\Response(
    //  *       response="400",
    //  *       description="BAD REQUEST"
    //  *   ),
    //  *   @SWG\Response(
    //  *       response="401",
    //  *       description="Unauthorized"
    //  *     ),
    //  *   @SWG\Response(
    //  *       response="409",
    //  *       description="Conflict"
    //  *   ),
    //  *   @SWG\Response(
    //  *       response="500",
    //  *       description="Internal Server Error"
    //  *     )
    //  * )
    //  * )
    //  */
    public function show($id)
    {
        $projects = auth()->user()->projects()->find($id);

        if (!$projects) {
            return response()->json([
                'success' => false,
                'message' => 'Project with id ' . $id . ' not found'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $projects->toArray()
        ], 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //  /**
    //  * @SWG\PUT(
    //  *   path="/api/projects/{id}",
    //  *   summary="Update A Project using current existed id",
    //  *   operationId="update",
    //  *   tags={"Projects"},
    //  *   security={
    //  *       {"Bearer": {}}
    //  *   },
    //  * @SWG\Parameter(
    //  *       name="id",
    //  *       in="path",
    //  *       required=true,
    //  *       type="string"
    //  *   ),
    //  *   @SWG\Parameter(
    //  *       name="projectName",
    //  *       in="formData",
    //  *       required=true,
    //  *       type="string"
    //  *   ),
    //  *   @SWG\Parameter(
    //  *       name="projectDescription",
    //  *       in="formData",
    //  *       required=true,
    //  *       type="string"
    //  *   ),
    //  *   @SWG\Parameter(
    //  *       name="startDate",
    //  *       in="formData",
    //  *       required=true,
    //  *       type="integer"
    //  *   ),
    //  *  @SWG\Parameter(
    //  *       name="endDate",
    //  *       in="formData",
    //  *       required=true,
    //  *       type="integer"
    //  *   ),
    //  *  @SWG\Parameter(
    //  *       name="projectOwner",
    //  *       in="formData",
    //  *       required=true,
    //  *       type="string"
    //  *   ),
    //  * *  @SWG\Parameter(
    //  *       name="projectStatus",
    //  *       in="formData",
    //  *       required=true,
    //  *       type="string"
    //  *   ),
    //  *   @SWG\Response(
    //  *     response=200,
    //  *     description="Successful"
    //  *   ),
    //  *   @SWG\Response(
    //  *       response="400",
    //  *       description="BAD REQUEST"
    //  *   ),
    //  *   @SWG\Response(
    //  *       response="401",
    //  *       description="Unauthorized"
    //  *     ),
    //  *   @SWG\Response(
    //  *       response="409",
    //  *       description="Conflict"
    //  *   ),
    //  *   @SWG\Response(
    //  *       response="500",
    //  *       description="Internal Server Error"
    //  *     )
    //  * )
    //  * )
    //  *
    //  */

    public function update(Request $request, $id)
    {
        $projects = auth()->user()->projects()->find($id);

        if (!$projects) {
            return response()->json([
                'success' => false,
                'message' => 'Projects with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $projects->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Projects could not be updated'
            ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects = auth()->user()->projects()->find($id);

        if (!$projects) {
            return response()->json([
                'success' => false,
                'message' => 'Projects with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $projects->update([
            'deleted_at' => Carbon::NOW(),
            'isActived' => false,
            'isDeleted' => true
            ]);

        if ($updated)
            return response()->json([
                'success' => true,
                'data' => $projects
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Projects could not be deleted'
            ], 500);
    }
}
