<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'taskName','estimateHour','actualHour','member_id','project_id','user_id','statuses',
        'isActive','isDeleted', 'created_at','updated_at','deleted_at'
    ];

    public function task(){
        return $this->hasMany(Subtask::class);
    }

    public function projects(){
        return $this->belongsTo(Projects::class);
    }
}
