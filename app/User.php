<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullName', 'email', 'password','userId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function projects()
    {
        return $this->hasMany(Projects::class);
    }
    
    public function unflaggedProject(){
        return DB::table('projects')
        ->whereNotNull('deleted_at')
        ->get();
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function subtasks()
    {
        return $this->hasMany(Subtask::class);
    }

    public function task_project(){
        return DB::table('tasks')
        ->join('projects', 'tasks.project_id', '=' , 'projects.id')
        ->select('tasks.taskName','tasks.estimateHour','tasks.actualHour','tasks.member_id',
        'tasks.project_id','tasks.statuses','projects.projectName')
        ->get();
    }

    public function task_subask(){
        return DB::table('tasks')
        ->join('subtasks', 'tasks.id', '=' , 'subtasks.id')
        ->select('tasks.taskName','tasks.estimateHour','tasks.actualHour','tasks.member_id',
        'tasks.project_id','tasks.statuses','projects.projectName')
        ->get();
    }
    

    public function subtask_task(){

    }

    public function members()
    {
        return $this->hasMany(Member::class);
    }

    // public function getTaskByProjectid(){
    //     return DB::table('tasks')
    //     ->join('projects', 'projects.id', '=', 'tasks.project_id')
    //     ->where('tasks.project_id' , request()->projects()->id)
    //     ->get();
    // }
}
