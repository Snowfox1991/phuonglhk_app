<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'memberId','memberName','teamId','created_at','updated_at','deleted_at'
    ];
}
