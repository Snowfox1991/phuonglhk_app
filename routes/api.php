<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\PassportController@login');
Route::post('register', 'Auth\PassportController@register');

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'Auth\PassportController@details');

    Route::resource('projects', 'ProjectController');
    Route::resource('tasks', 'TaskController');

    Route::resource('subtasks', 'SubtaskController');

    Route::resource('members', 'MemberController');

    Route::get('deleted_projects', 'ProjectController@index');

    Route::get('task_project', 'TaskController@getTaskByProject');
    Route::get('subtask_task', 'SubtaskController@getSubtaskByTask');
    
});
