<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('taskName');
            $table->integer('estimateHour');
            $table->integer('actualHour')->nullable();
            $table->integer('member_id')->nullable()->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('statuses', ['NOT STARTED','IN PROGRESSING','COMPLETED','DECLINED','CANCELLED']);
            $table->boolean('isActive')->default(1);
            $table->boolean('isDeleted')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('project_id')
            ->references('id')
            ->on('projects');

            $table->foreign('user_id')
            ->references('id')
            ->on('users');

            $table->foreign('member_id')
            ->references('id')
            ->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['project_id']);
            $table->dropForeign(['member_id']);
            $table->dropColumn(['user_id', 'project_id', 'member_id']);
        });
    }
}
