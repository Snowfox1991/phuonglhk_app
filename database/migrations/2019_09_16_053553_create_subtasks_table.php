<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subtaskName');
            $table->integer('estimateHour');
            $table->integer('actualHour')->nullable();
            $table->integer('member_id')->nullable()->unsigned();
            $table->integer('task_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('taskStatus', ['NOT STARTED','IN PROGRESSING','COMPLETED','DECLINED','CANCELLED']);
            $table->boolean('isActive')->default(1);
            $table->boolean('isDeleted')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('task_id')
            ->references('id')
            ->on('tasks');

            $table->foreign('member_id')
            ->references('id')
            ->on('members');

            $table->foreign('user_id')
            ->references('id')
            ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtasks');
        Schema::table('subtasks', function (Blueprint $table) {
            $table->dropForeign(['task_id']);
            $table->dropForeign(['member_id']);
            $table->dropForeign(['user_id']);
            $table->dropColumn(['task_id', 'member_id','user_id']);
        });
    }
}
