<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('projectName');
            $table->text('projectDescription');
            $table->datetime('startDate');
            $table->datetime('endDate');
            $table->string('projectOwner');
            $table->integer('user_id')->unsigned();
            $table->enum('projectStatus', ['NOT STARTED', 'IN PROGRESS', 'COMPLETED', 'FAILED']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
            ->references('id')
            ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });
        // Schema::dropIfExists('projects');
        // $table->dropForeign(['user_id']);
    }
}
